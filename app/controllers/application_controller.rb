class ApplicationController < ActionController::Base
  include Clearance::Controller
  protect_from_forgery with: :exception

  def require_logged_user
    unless signed_in?
      redirect_to root_path
    end
  end

  def require_approved_user
    unless current_user.approved
      redirect_to edit_user_path(current_user)
    end
  end
end
