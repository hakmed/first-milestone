class StripeCustomersController < ApplicationController
  before_action :require_logged_user

  def new
  end

  def create
    customer = Stripe::Customer.create(
      :source => params[:stripeToken],
      :description => "User #{current_user.id} with role #{current_user.role}"
    )
    current_user.update(stripe_customer_id: customer.id)

    redirect_to params[:callback] || root_path
  end
end
