class Patient::ChatRoomsController < Patient::ApplicationController
  # before_action :set_opentok

  def create
    # session = @opentok.create_session location: request.remote_addr
    @room = ChatRoom.new(chat_room_params)
    # @room.session_id = session.session_id
    @room.patient = current_user

    if @room.save
      redirect_to chat_room_path(@room)
    else
      p @room.errors
    end
  end

  private

  def chat_room_params
    params.require(:chat_room).permit(:doctor_id)
  end

  # def set_opentok
  #   @opentok ||= OpenTok::OpenTok.new ENV['OPENTOK_KEY'], ENV['OPENTOK_SECRET']
  # end
end
