class Patient::PatientsController < Patient::ApplicationController
  skip_before_action :require_logged_user
  skip_before_action :require_approved_user
  skip_before_action :require_patient

  def new
    @patient = Patient.new
  end

  def create
    @patient = Patient.new(patient_params)
    @patient.type = Patient

   if @patient.save
     sign_in @patient
     redirect_back_or root_path
   else
     p '_+_+_+_+_'
     p @patient.errors
     render :new
   end
  end

  private

  def patient_params
    params.require(:patient).permit(:zip_code, :name, :email, :password, :phone)
  end
end
