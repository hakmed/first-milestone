class Patient::ApplicationController < ApplicationController
  before_action :require_logged_user
  before_action :require_approved_user
  before_action :require_patient

  protected

  def require_patient
    unless current_user.is_a? Patient
      redirect_to root_path, alert: 'You should be a patient to get this page'
    end
  end
end
