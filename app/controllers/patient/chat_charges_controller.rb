class Patient::ChatChargesController < Patient::ApplicationController
  before_action :find_chat_room

  def new
    # @paypal_path = paypal_patient_chat_room_chat_charges_path(@chat_room)
    charge_path = stripe_patient_chat_room_chat_charges_path(@chat_room)
    @stripe_path = if current_user.stripe_customer_id
      charge_path
    else
      new_stripe_customer_path(callback: charge_path)
    end
  end

  def stripe
    customer_id = current_user.stripe_customer_id

    p ')_)_)_)'
    p customer_id
    p current_user
    begin
      Stripe::Charge.create(
        amount: (@chat_room.doctor.video_chat_rate * 100).to_i, # in cents
        currency: "usd",
        customer: customer_id,
        description: "Charge for consultation on WEEDOCTOR"
      )

      @chat_room.update(payed: true)
      redirect_to chat_room_activate_path(@chat_room)
    rescue Stripe::CardError => e
      p '_________'
      p e
      current_user.update!(stripe_customer_id: nil)
      redirect_to new_patient_chat_room_chat_charge_path(@chat_room), alert: 'Your card is invalid.'
    end
  end

  def paypal
    attrs = get_paypal_attrs
    @payment = PayPalService.new(*attrs)
    if @payment.accept
      redirect_to extract_link(@payment.response)
    else
      redirect_to attrs[1], notice: 'Something went wrong '
    end
  end

  def paypal_success
    payment = PayPal::SDK::REST::Payment.find(params[:paymentId])
    payment.execute(payer_id: params[:PayerID])
    @chat_room.update(payed: true)
    redirect_to chat_room_path(@chat_room)
  end

  private

  def find_chat_room
    @chat_room = ChatRoom.includes(:doctor).find(params[:chat_room_id])
  end

  def get_paypal_attrs
    success = request.base_url + paypal_success_patient_chat_room_chat_charges_path(@chat_room)
    cancel = request.base_url + new_patient_chat_room_chat_charge_path(@chat_room)
    amount = @chat_room.doctor.video_chat_rate

    [success, cancel, amount]
  end

  def extract_link(data)
    data.links.find { |link| link.rel == 'approval_url' }.href
  end
end
