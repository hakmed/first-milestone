class Patient::HomeController < Patient::ApplicationController

  def show
    ids = Doctor.pluck(:id).map { |id| id if $redis_onlines.exists(id) }.compact
    @doctors = Doctor.where(id: ids)
  end
end
