class Patient::DoctorsController < Patient::ApplicationController
  before_action :require_logged_user
  before_action :require_approved_user

  def show
    @doctor = User.find(params[:id])
  end
end
