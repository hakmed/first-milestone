class ChatRoomsController < ApplicationController
  before_action :require_logged_user
  before_action :require_approved_user
  before_action :find_room
  before_action :require_proper_user, only: [:show]
  before_action :set_opentok
  before_action :require_doctor, only: :activate

  def show
    @opposite_user = current_user == @room.doctor ? @room.patient : @room.doctor
    @tok_token = @opentok.generate_token(@room.session_id) if @room.payed && @room.session_id
  end

  def activate
    unless @room.payed
      redirect_to new_patient_chat_room_chat_charge_path(@room)
      return
    end

    session = @opentok.create_session location: request.remote_addr
    @room.session_id = session.session_id
    @room.save!
    redirect_to chat_room_path(@room)
  end

  def deactivate
    @room.update!(session_id: nil)
    redirect_to chat_room_path(@room)
  end

  private

  def set_opentok
    @opentok ||= OpenTok::OpenTok.new ENV['OPENTOK_KEY'], ENV['OPENTOK_SECRET']
  end

  def find_room
    @room = ChatRoom.includes(:messages).find(params[:id] || params[:chat_room_id])
  end

  def require_proper_user
    unless current_user == @room.doctor || current_user == @room.patient
      redirect_to root_path
    end
  end

  def require_doctor
    redirect_to chat_room_path(@room) if current_user.doctor?
  end
end
