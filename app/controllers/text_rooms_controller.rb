class TextRoomsController < ApplicationController
  before_action :require_logged_user
  before_action :require_approved_user

  def show
    @text_room = TextRoom.includes(messages: [:author]).find(params[:id])
    redirect_to root_path unless current_user == @text_room.doctor || current_user == @text_room.patient
    @opposite_user = current_user == @text_room.doctor ? @text_room.patient : @text_room.doctor
  end

  def create
    @text_room = TextRoom.new(text_room_params)
    @text_room.patient = current_user

    if @text_room.save
      WebNotificationsChannel.broadcast_to(
        @text_room.doctor,
        body: TextRoomsController.render(partial: 'notifications/invite_to_text_chat', locals: { patient: current_user, text_room: @text_room })
      )
      redirect_to text_room_path(@text_room)
    else
      redirect_to doctors_path
    end
  end

  private

  def text_room_params
    params.require(:text_room).permit(:doctor_id)
  end
end
