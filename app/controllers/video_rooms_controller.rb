class VideoRoomsController < ApplicationController
  before_action :require_logged_user
  before_action :require_approved_user
  before_action :set_opentok

  def show
    @room = VideoRoom.find(params[:id])
    redirect_to root_path unless current_user == @room.doctor || current_user == @room.patient
    @opposite_user = current_user == @room.doctor ? @room.patient : @room.doctor
    @tok_token = @opentok.generate_token(@room.session_id)
  end

  def create
    session = @opentok.create_session location: request.remote_addr

    @room = VideoRoom.new(video_room_params)
    @room.session_id = session.session_id

    if @room.save
      WebNotificationsChannel.broadcast_to(
        @room.patient,
        body: VideoRoomsController.render(partial: 'notifications/invite_to_video_chat', locals: { doctor: @room.doctor, room: @room })
      )
      redirect_to video_room_path(@room)
    end
  end

  private

  def set_opentok
    @opentok ||= OpenTok::OpenTok.new ENV['OPENTOK_KEY'], ENV['OPENTOK_SECRET']
  end

  def video_room_params
    params.require(:video_room).permit(:doctor_id, :patient_id)
  end

end
