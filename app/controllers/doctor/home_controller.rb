class Doctor::HomeController < Doctor::ApplicationController
  def show
    @chats = ChatRoom.includes(:messages).where(doctor: current_user).limit(5).map do |chat|
      chat if chat.messages.any? && chat.messages.first.author != current_user
    end.compact

    @number_of_patients = @chats.map(&:patient_id).uniq.count
  end
end
