class Doctor::ApplicationController < ApplicationController
  before_action :require_logged_user
  before_action :require_approved_user
  before_action :require_doctor

  protected

  def require_doctor
    unless current_user.is_a? Doctor
      redirect_to root_path, alert: 'You should be a doctor to get this page'
    end
  end
end
