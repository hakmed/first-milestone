class Doctor::DoctorsController < Doctor::ApplicationController
  skip_before_action :require_logged_user, only: [:new, :create]
  skip_before_action :require_approved_user, only: [:new, :create]
  skip_before_action :require_doctor, only: [:new, :create]
  before_action :find_doctor, only: [:edit, :update]

  def new
    @doctor = Doctor.new
  end

  def create
    @doctor = Doctor.new(doctor_params)
    @doctor.type = Doctor

   if @doctor.save
     sign_in @doctor
     redirect_back_or root_path
   else
     render :new
   end
  end

  def edit
  end

  def update
    if @doctor.update(doctor_params)
      redirect_to root_path
    else
      render :edit
    end
  end


  private

  def find_doctor
    @doctor = Doctor.find(params[:id])
  end

  def doctor_params
    params.require(:doctor).permit(
      :zip_code, :name, :email, :password, :phone,
      :province, :num, :med_id, :studio_address, :city,
      :video_chat_rate
    )
  end
end
