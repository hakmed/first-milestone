class MessagesController < ApplicationController
  def create
    @message = Message.new(message_params)
    @message.chat_room_id = params[:chat_room_id]

    if @message.save
      ActionCable.server.broadcast(
        "chat_room:#{@message.chat_room_id}",
        message: MessagesController.render(partial: 'messages/message', locals: { message: @message })
      )
    end
  end

  private

  def message_params
    params.require(:message).permit(:author_id, :body)
  end
end
