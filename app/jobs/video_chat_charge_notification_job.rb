class VideoChatChargeNotificationJob < ApplicationJob
  queue_as :default

  def perform(video_room)
    WebNotificationsChannel.broadcast_to(
      video_room.doctor,
      body: "Patient #{video_room.patient.name} was successfully charged for video chat."
    )
    WebNotificationsChannel.broadcast_to(
      video_room.patient,
      body: "You were successfully charged for #{video_room.doctor.video_chat_rate.to_i}$"
    )
  end
end
