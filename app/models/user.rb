class User < ApplicationRecord
  include Clearance::User

  scope :approved, -> { where(approved: true) }

  def doctor?
    self.class.to_s.underscore == 'doctor'
  end

  def patient?
    self.class.to_s.underscore == 'patient'
  end

  def role
    self.class.to_s.underscore
  end

  def appear
    $redis_onlines.set(id, nil, ex: 5*60)
  end

  def online?
    $redis_onlines.exists(self.id)
  end

  def self.authenticate(email, password)
    if user = find_by_normalized_email(email)
      if password.present? && user.authenticated?(password)
        user
      end
    end
  end
end
