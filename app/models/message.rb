class Message < ApplicationRecord
  default_scope -> { order(created_at: :desc) }
  belongs_to :author, class_name: User
  belongs_to :chat_room

  validates :body, :chat_room, :author, presence: true
end
