class ChatRoom < ApplicationRecord
  belongs_to :patient, class_name: Patient
  belongs_to :doctor, class_name: Doctor

  has_many :messages, dependent: :destroy

  validates :doctor, :patient, presence: true

  after_update :send_notifications, if: -> (chat_room) {
                                      chat_room.payed_change == [false, true]
                                    }
  def send_notifications
    VideoChatChargeNotificationJob.set(wait: 5.seconds).perform_later(self)
    WebNotificationsChannel.broadcast_to(
      doctor,
      body: ChatRoomsController.render(
        partial: 'notifications/invite_to_chat',
        locals: { patient: patient, room: self }
      )
    )
  end
end
