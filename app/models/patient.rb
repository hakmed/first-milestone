class Patient < User
  validates :name, :email, :zip_code, presence: true
  before_save :set_approvement
  before_create :set_patient_id

  def set_approvement
    if zip_code && email && name && !doctor?
      self.approved = true
    end
  end

  def set_patient_id
    self.patient_id = "ID" + SecureRandom.hex(6).upcase
  end

  def password_optional?
    true
  end

  def authenticated?(password)
    password == patient_id
  end
end
