class VideoRoom < ApplicationRecord
  belongs_to :doctor, class_name: Doctor
  belongs_to :patient, class_name: Patient

  validates :patient, :doctor, :session_id, presence: true
end
