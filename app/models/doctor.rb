class Doctor < User
  validates :name, :province, :num, :med_id, :studio_address, :city, :zip_code, presence: true
  before_create :set_patient_id
  before_save :set_approvement

  def set_approvement
    if zip_code && email && name
      self.approved = true
    end
  end

  def set_patient_id
    self.patient_id = "ID" + SecureRandom.hex(6).upcase
  end

  def password_optional?
    true
  end

  def authenticated?(password)
    password == patient_id
  end
end
