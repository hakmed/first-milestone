module ApplicationHelper
  def is_active?(path)
    return 'active' if request.env['PATH_INFO'] == path
  end

  def availability_status(doctor)
    btn = if doctor.online?
      ['online', 'success']
    else
      ['offline', 'danger']
    end

    content_tag :button, class: "btn btn-#{btn.last} btn-xs" do
       btn.first
    end
  end
end
