class PayPalService
  def initialize(success, cancel, amount)
   @payment = PayPal::SDK::REST::Payment.new(data(success, cancel, amount))
  end

  def accept
    @payment.create
  end

  def response
    @payment
  end

  private

  def data(success, cancel, amount)
    {
      intent: 'sale',
      payer: {
        payment_method: 'paypal' },
      redirect_urls: {
        return_url: success,
        cancel_url: cancel },
      transactions: [ {
        amount: {
          total: amount,
          currency: 'USD' },
        description: 'subscription' } ]
    }
  end
end
