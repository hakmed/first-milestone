class ChatRoomChannel < ApplicationCable::Channel

  def follow(data)
    stop_all_streams
    stream_from "chat_room:#{data['chat_room'].to_i}"
  end

  def unfollow
    stop_all_streams
  end

  def receive(data)
    ActionCable.server.broadcast("chat_room:#{params[:chat_room]}", data)
  end
end
