class AppearanceChannel < ApplicationCable::Channel
  def subscribed
    current_user.appear
  end

  def appear
    current_user.appear
  end
end
