App.cable.subscriptions.create "AppearanceChannel",
  connected: ->
    @install()
    @appear()

  rejected: ->
    @uninstall()

  appear: ->
    @perform("appear")

  install: ->
    $(document).on "page:change.appearance", =>
      @appear()

  uninstall: ->
    $(document).off(".appearance")
