App.chatRoom = App.cable.subscriptions.create "ChatRoomChannel",
  collection: -> $(".messages")

  connected: ->
    # FIXME: While we wait for cable subscriptions to always be finalized before sending messages
    setTimeout =>
      @followCurrentMessage()
      @installPageChangeCallback()
    , 1000

  received: (data) ->
    console.log data
    @collection().prepend(data.message)
    $("#new_message input[name='message[body]']").val('')

  followCurrentMessage: ->
    if chatRoom = @collection().data('chat-room-id')
      @perform 'follow', chat_room: chatRoom
    else
      @perform 'unfollow'

  installPageChangeCallback: ->
    unless @installedPageChangeCallback
      @installedPageChangeCallback = true
      $(document).on 'turbolinks:render', -> App.chatRoom.followCurrentMessage()
