App.cable.subscriptions.create "WebNotificationsChannel",
  received: (data) ->
    toastr.options.preventDuplicates = false
    toastr.options.timeOut = 30000
    toastr.info(data.body)
