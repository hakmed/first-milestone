var apiKey,
    sessionId,
    token;

$(document).on('turbolinks:load', function() {
  var videos = $('.video');
  apiKey = videos.data('api-key');
  sessionId = videos.data('session-id');
  token = videos.data('token');

  if(apiKey && sessionId && token) {
    initializeSession();
  }
});

function initializeSession() {
  var session = OT.initSession(apiKey, sessionId);

  // Subscribe to a newly created stream
  session.on('streamCreated', function(event) {
    session.subscribe(event.stream, 'subscriber', {
      insertMode: 'append',
      width: '100%',
      height: '100%'
    });
  });

  session.on('sessionDisconnected', function(event) {
    console.log('You were disconnected from the session.', event.reason);
  });

  // Connect to the session
  session.connect(token, function(error) {
    // If the connection is successful, initialize a publisher and publish to the session
    if (!error) {
      var publisher = OT.initPublisher('publisher', {
        insertMode: 'append',
        width: '100%',
        height: '100%'
      });

      session.publish(publisher);
    } else {
      console.log('There was an error connecting to the session: ', error.code, error.message);
    }
  });

  $(document).on('turbolinks:render', function(){ session.disconnect(); })
}
