Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  constraints Clearance::Constraints::SignedIn.new { |user| user.patient? } do
    root to: "patient/home#show"
  end
  constraints Clearance::Constraints::SignedIn.new { |user| user.doctor? } do
    root to: "doctor/home#show"
  end

  constraints Clearance::Constraints::SignedOut.new do
    root to: "home#show"
  end
  resources :stripe_customers, only: [:new, :create]
  resources :chat_rooms, only: [:show] do
    get 'activate' => 'chat_rooms#activate'
    get 'deactivate' => 'chat_rooms#deactivate'
    resources :messages, only: [:create]
  end
  namespace :patient do
    resources :patients, only: [:new, :create]
    resources :doctors, only: [:show]
    resources :chat_rooms, only: [:create] do
      resources :chat_charges, only: [:new] do
        get 'stripe' => 'chat_charges#stripe', on: :collection
        post 'stripe' => 'chat_charges#stripe', on: :collection
        get 'paypal' => 'chat_charges#paypal', on: :collection
        get 'paypal_success' => 'chat_charges#paypal_success', on: :collection
      end
    end
  end

  namespace :doctor do
    resources :doctors, only: [:new, :create, :edit, :update]
  end
  get 'users/self/edit' => 'users#edit'

end
