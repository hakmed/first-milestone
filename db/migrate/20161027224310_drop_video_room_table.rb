class DropVideoRoomTable < ActiveRecord::Migration[5.0]
  def self.up
    drop_table :video_rooms
  end

  def self.down
  end
end
