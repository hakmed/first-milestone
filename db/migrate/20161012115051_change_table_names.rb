class ChangeTableNames < ActiveRecord::Migration[5.0]
  def change
    rename_column :text_rooms, :inviter_id, :patient_id
    rename_column :text_rooms, :invited_id, :doctor_id
  end
end
