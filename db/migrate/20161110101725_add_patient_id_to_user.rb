class AddPatientIdToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :patient_id, :string
    add_index :users, :patient_id
  end
end
