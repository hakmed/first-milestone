class CreateTextChatInvitations < ActiveRecord::Migration[5.0]
  def change
    create_table :text_rooms do |t|
      t.integer :inviter_id
      t.integer :invited_id

      t.timestamps
    end

    add_index :text_rooms, :invited_id
    add_index :text_rooms, :inviter_id
  end
end
