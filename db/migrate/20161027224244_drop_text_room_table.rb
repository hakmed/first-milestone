class DropTextRoomTable < ActiveRecord::Migration[5.0]
  def self.up
    drop_table :text_rooms
  end

  def self.down
  end
end
