class AddDoctorFieldsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :province, :string
    add_column :users, :num, :string
    add_column :users, :med_id, :string
    add_column :users, :studio_address, :string
    add_column :users, :city, :string

    add_index :users, :type
  end
end
