class AddPrescriptionRateToDoctor < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :prescription_rate, :float, default: 0.5
  end
end
