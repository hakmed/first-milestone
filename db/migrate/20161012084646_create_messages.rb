class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.string :body
      t.integer :author_id
      t.integer :text_room_id

      t.timestamps
    end

    add_index :messages, :author_id
    add_index :messages, :text_room_id
  end
end
