class AddVideoChatRateToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :video_chat_rate, :float, default: 1
  end
end
