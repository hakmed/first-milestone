class CreateVideoRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :video_rooms do |t|
      t.string :session_id
      t.integer :doctor_id
      t.integer :patient_id

      t.timestamps
    end

    add_index :video_rooms, :doctor_id
    add_index :video_rooms, :patient_id
  end
end
