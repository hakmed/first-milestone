class RenameMessagesForeignKey < ActiveRecord::Migration[5.0]
  def self.up
    Message.destroy_all

    rename_column :messages, :text_room_id, :chat_room_id
  end

  def self.down
    rename_column :messages, :chat_room_id, :text_room_id
  end
end
