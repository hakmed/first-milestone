class AddRoleToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :role, :integer
    add_column :users, :approved, :boolean, default: false
    add_index :users, :approved
  end
end
