class CreateChatRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :chat_rooms do |t|
      t.integer :patient_id
      t.integer :doctor_id
      t.string :session_id
      t.boolean :payed, default: false

      t.timestamps
    end

    add_index :chat_rooms, :patient_id
    add_index :chat_rooms, :doctor_id
  end
end
