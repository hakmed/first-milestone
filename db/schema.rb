# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161114161404) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "chat_rooms", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "doctor_id"
    t.string   "session_id"
    t.boolean  "payed",      default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["doctor_id"], name: "index_chat_rooms_on_doctor_id", using: :btree
    t.index ["patient_id"], name: "index_chat_rooms_on_patient_id", using: :btree
  end

  create_table "messages", force: :cascade do |t|
    t.string   "body"
    t.integer  "author_id"
    t.integer  "chat_room_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["author_id"], name: "index_messages_on_author_id", using: :btree
    t.index ["chat_room_id"], name: "index_messages_on_chat_room_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "zip_code"
    t.string   "phone"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "email",                                          null: false
    t.string   "encrypted_password", limit: 128
    t.string   "confirmation_token", limit: 128
    t.string   "remember_token",     limit: 128,                 null: false
    t.integer  "role",                           default: 0
    t.boolean  "approved",                       default: false
    t.string   "type"
    t.string   "province"
    t.string   "num"
    t.string   "med_id"
    t.string   "studio_address"
    t.string   "city"
    t.string   "stripe_customer_id"
    t.float    "video_chat_rate",                default: 1.0
    t.string   "patient_id"
    t.float    "prescription_rate",              default: 0.5
    t.index ["approved"], name: "index_users_on_approved", using: :btree
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["patient_id"], name: "index_users_on_patient_id", using: :btree
    t.index ["remember_token"], name: "index_users_on_remember_token", using: :btree
    t.index ["type"], name: "index_users_on_type", using: :btree
  end

end
